<rrplayer>
  <div class="container">
    <div class="setMode">
      <div class="live { live-off: !live }">
      	<div class="liveButton" onclick={ backToLive }>
	        <span title="Go back to the LiveStream">LIVE</span>
	     </div>
	     <div class="channel">
	     	<div class="channel1" onclick={ setChannel1 }>
	        <span>1</span>
	        </div>
	        <div class="channel2" onclick={ setChannel2 }>
	        <span>2</span>
	        </div>
	     </div>
      </div>
      <div class="archive { archive-off: live}">
        <span>ARCHIVE</span>
      </div>
    </div>
    <div id="playpause" onclick={ playpause }>
      <div class={ hideloading: !loading }>
      </div>
      <span id="playpausesymbol" class={ hidesymbol: loading }>{ playing? 'll' : '&#9654;&#xFE0E' }</span>
    </div>
    <div class="info">
      <div class="tracktitle">
        <span class="title">{ tracktitle }</span>
      </div>
      <div class="timeLabel">
        <span class="currentTimeLabel">{ timeLabel }</span>
      </div>
      <div class="trackprogress { scrollable: scrollable }" onclick = { scrollPodcast }>
        <div class="progress">
        </div>
      </div>
    </div>
  </div>
  <audio id="player" src={ playurl }></audio>

  <script>

    this.playing = false
    this.loading = false
    this.live = true
    this.tracktitle = 'Press play to start streaming 🚀'
    this.timeLabel = ''
    this.liveUrl1 = 'https://gintonic.tk:8001/mirror'
    this.liveUrl2 = 'https://gintonic.tk:8001/stream'
    this.playurl = this.liveUrl1
    this.scrollable = false

    window.rrplayer = this
    this.rrbutton = undefined

	this.on('mount', function() {
		this.updateTitle();
	})
	updateTitle() {
		this.getStreamTitle(this)
	}
	
     
     $(function() {
        console.log( "window loaded" );
        self.tracktitle = 'play stream!'
        self.update;
        setInterval(this.getStreamTitle, 100);
        this.getStreamTitle(this)
    });


	/* setInterval(function() {
		getStreamTitle(this);
	}, 1000); */

    if (/iPad|iPhone|iPod/.test(navigator.userAgent)) {
        this.player.autoplay = true;
    }

    // scrollPodcast() {
    //   if (!this.live) {
    //     $('.trackprogress').click($.proxy(function (event) {
    //       var parentOffset = $('.trackprogress').offset();
    //       var newAudioPercentage = (event.pageX - parentOffset.left)/$('.trackprogress').css('width').replace('px','');
    //       this.setPodcastTime(newAudioPercentage);
    //     },this));
    //   }
    //   // TO DO: Handle "ended" event on audio player
    //   // if we want to use onclick, there should not be an event listener?
    // }

    scrollPodcast() {
      if (!this.live) {
        var parentOffset = $('.trackprogress').offset();
        var newAudioPercentage = (event.pageX - parentOffset.left)/$('.trackprogress').css('width').replace('px','');
        this.setPodcastTime(newAudioPercentage);
      }
      // TO DO: Handle "ended" event on audio player
      // if we want to use onclick, there should not be an event listener?
    }

    setPodcastTime(newPtc) {
      this.player.currentTime = this.player.duration * newPtc;
    }

    playPodcast(url,title,button) {
    	clearInterval(this.titleUpdater)
      this.rrbutton = button
      //console.log("called")
      if (url == this.playurl) {
        this.playpause()
      } else {
        this.player.pause()
        this.playing = false
        this.playurl = url
        this.tracktitle = title
        this.live = false
        //this.player.addEventListener('timeupdate',this.updateProgress);
        $('rrplayer audio').on('timeupdate',this.updateProgress)
//         $('rrplayer audio').on('timeupdate',this.updateTime)
// 		setInterval(this.updateTime(this){ }, 1000);	
        this.scrollable = true
        this.update()
        this.playpause()
      }
    }

    updateTime() {
      self.timeLabel = Math.floor(rrplayer.player.currentTime);
	  self.update
        //this.duration = Math.floor(rrplayer.player.duration)

    }

    updateProgress() {
      //percentage = (parseFloat(this.player.currentTime/this.player.duration*100,10)).toFixed(2);
      percentage = (parseFloat(rrplayer.player.currentTime/rrplayer.player.duration*100,10)).toFixed(2);
      $('.progress').css('width',percentage + '%');
    }

    playLive() {
      if (this.playing) {
        this.player.pause()
        this.player.src = ''
        this.title = undefined
      } else {
        //this.loading = true
        //this.player.addEventListener('loadeddata', this.playWhenLoaded.bind(this), false)
        //this.player.src = this.liveUrl1
        //this.playWhenLoaded()
        this.player.play()
        this.getStreamTitle(this)
        this.titleUpdater = setInterval(this.updateTitle, 5000)
      }
      this.playing = !this.playing
      if (this.rrbutton) {
        this.rrbutton.playing = false
        this.rrbutton.update()
      }
      this.scrollable = false
    }

    playWhenLoaded() {
      this.player.play()
      this.loading = false
      this.update()
    }

    playpause() {
      if (this.live) {
        this.playLive()
      } else {
        if (this.playing) {
          this.player.pause()
        } else {
          this.player.play()
        }
        this.playing = !this.playing
        this.rrbutton.playing = !this.rrbutton.playing
        this.update()
        this.rrbutton.update()
      }
    }

    backToLive() {
      if (!this.live) {
        this.live = true
        this.playing = false
        this.playurl = this.liveUrl1
        this.update()
        this.playLive()
      }
    }
    setChannel1() {
    	if (this.live) {
    		this.player.pause
        	this.playing = !this.playing
        	this.playurl = this.liveUrl1
	        this.update()
	        this.playLive()
    	}
    }
	setChannel2() {
	    	if (this.live) {
    		this.player.pause
        	this.playing = !this.playing
	        	this.playurl = this.liveUrl2
		        this.update()
		        this.playLive()
	    	}
	}

    getStreamTitle(self) {
      var request = new XMLHttpRequest();
      request.open('GET', 'https://gintonic.tk:8001/status-json.xsl', true);
      request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
          // Success!
          var data = JSON.parse(request.responseText);
          if (data.icestats.source.title == "ROCKET Radio Offline") {
            self.tracktitle = 'ROCKET Radio is currently offline!'
            self.playing = false
          } else {
            self.tracktitle = data.icestats.source[0].title
          }
          self.update()
        } else {
          // We reached our target server, but it returned an error
          self.tracktitle = "I don't know the title of this track 😢"
          self.update()
        }
      };

      request.onerror = function() {
        // There was a connection error of some sort
        self.tracktitle = "APOLLO 13 🚀💥"
        self.update()
      };

      request.send();
    }

  </script>

  <style>
    rrplayer {
      position: fixed;
      bottom: 0;
      z-index: 99999;
    }

    .container {
      font-family: 'Futura', sans-serif;
      background-color: black;
      position: relative;
      user-select: none;
      bottom: 0;
      left: 0;
      z-index: 9999;
      //display: flex;
      display: table;
      //flex-direction: row;
      width: 100%;
      overflow: hidden;
    }
    audio {
      width: 100%;
      display: inline;
    }
    .setMode {
      //display: inline-block;
      min-width: 100px;

      display: table-cell;
      position: relative;
      cursor: pointer;
      vertical-align: middle;
      height: 100%;
    }
    .live {
      background-color: red;
      overflow: hidden;
      color: white;
      width: 100%;
      //text-align: center;
      vertical-align: middle;
      height: 50%;
    }
    .live span {
      //font-weight: 600;
    }
    .live-off {
      background-color: black;
      color: rgb(170, 170, 170);
      //text-align: center;
      //font-weight: 600;
      height: 50%;
    }
    .live-off:hover {
      background-color: red;
      color: white;
    }
    .liveButton {
    	padding-left: 10px;
      display: inline-flex;
      float: left;
    }
    .channel {
      display: inline-flex;
      float: right;
      width: 40%;
    }.channel1 {
      display: table-cell;
      width: 50%;
      text-align: center;
    }.channel2 {
      display: table-cell;
      width: 50%;
      text-align: center;
      padding: 0 5 0 1;
    }
    .archive {
      //background-color: rgb(18, 171, 205);
      color: rgb(18, 171, 205);
      text-align: center;
      vertical-align: middle;
      height: 50%;
      border-top: 1px solid rgba(255, 255, 255, .3);
    }
    .archive span {
      //font-weight: 600;
    }
    .archive-off {
      //background-color: rgba(18, 171, 205, 0.70);
      color: rgb(170, 170, 170);
      text-align: center;
      //font-weight: 600;
      height: 50%;
    }
    #playpause {
      font-size: 1.7rem;
      color: white;
      text-align: center;
      margin: 0 2px 0 5px;
      position: relative;

      display: table-cell;
      vertical-align: middle;
      min-width: 60px;
      height: 100%;
      border-left: 1px solid rgba(255, 255, 255, .3);
      border-right: 1px solid rgba(255, 255, 255, .3);
    }
    .tracktitle {
      //display: block;
	  white-space: nowrap;
	  overflow: hidden;
      margin-left: 10px;
      width: 100%;
      height: 100%;
      vertical-align: middle;
    }
    .timeLabel {
      //display: block;

      margin-left: 10px;
      width: 100%;
      height: 100%;
      vertical-align: middle;
    }
    .info {
      width: 100%;

      display: table-cell;
      vertical-align: middle;
      height: 100%;
      position: relative;
    }
    .tracktitle span {
      color: white;
      //animation: marquee 5s linear infinite;
    }
    .title {
      text-overflow: ellipsis;
    }
    .trackprogress {

      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      display: table-cell;
      position: absolute;
    }
    .scrollable {
      cursor: pointer;
    }
    .progress {
      height: 100%;
      background: rgba(255, 255, 255, .3);
      width: 0%;
    }
    .marquee {
    margin: 0 auto;
    white-space: nowrap;
    overflow: hidden;
    box-sizing: border-box;
	}
	.marquee span {
	    display: inline-block;
	    padding-left: 0;  /* show the marquee just outside the paragraph */
	    animation: marquee 15s linear infinite;
	}
	.marquee span:hover {
	    animation-play-state: paused
	}
	/* Make it move */
	@keyframes marquee {
	    0%   { transform: translate(0, 0); }
	    100% { transform: translate(-100%, 0); }
	}
    
/* 
    @media only screen
      and (min-device-width: 375px)
      and (max-device-width: 667px)
      and (-webkit-min-device-pixel-ratio: 2) {
        .container {
  		  font-size: 1.2rem;
          width: 100%;
        }
        .info {
          //position: relative;
          overflow: hidden;
        }
        #playpause {
          width: 54px;
          height: 50px;
        }
        #playpausesymbol {
          font-size: 1.7rem;
        }
        .live {
          line-height: 1.2rem;
          font-size: 1.2rem;
          height: 50%;
        }
        .live span {
          line-height: 1.2rem;
        }
        .archive {
          line-height: 1.2rem;
          font-size: 1.2rem;
          //display: block;
        }
        .archive span {
          line-height: 1.2rem;
        }
        .tracktitle {
          height: 25px;
        }
        .tracktitle {
          font-size: 1.2rem;
          //display: block;
          //position: absolute;
          width: 600%;
          animation: marquee 15s linear infinite;
        }
        .timeLabel {
        	//display: block:
        	height: 50%;
        	font-size: 1.2rem;
        }
        .title {
          float: left;
        }
        .trackprogress {
          height: 50px;
        }

    } */

    /*LOADER*/
    .hidesymbol {
      visibility: hidden;
    }

    .hideloading {
      visibility: hidden;
    }


  </style>

</rrplayer>
