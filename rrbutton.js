<rrbutton>

  <div>
    <span class="rrplay" onclick={ playPodcast }>{ playing? 'PAUSE' : 'PLAY' }</span>
    <a href="{ downloadUrl }" download="RRpodcast.mp3" class="rrdownload">DOWNLOAD</a>
  </div>

  <script>
    //this.playing = false
    this.playing = false
    this.started = false
    this.downloadUrl = ''
    this.title = ''
    this.on('mount', function() {
      // right after the tag is mounted on the page
      this.getPodcastUrl(this);
    })

    playPodcast() {
      if (!this.started) {
        window.rrplayer.playPodcast(this.downloadUrl,this.title,this)
      }
      if (this.playing) {
        console.log('pause')
      } else {
        console.log('play')
      }
      //this.playing = !this.playing
    }

    getPodcastUrl(self) {
      var request = new XMLHttpRequest();
      request.open('GET', window.location.href + '?format=json-pretty', true);

      request.onload = function() {
        if (request.status >= 200 && request.status < 400) {
          // Success!
          var data = JSON.parse(request.responseText);
          console.log(data.item.sourceUrl);
          self.title = data.item.title;
          self.downloadUrl = data.item.sourceUrl;
          self.update();
        } else {
          // We reached our target server, but it returned an error
          console.log('no title?')
          return false;
        }
      };

      request.onerror = function() {
        // There was a connection error of some sort
        console.log('req err');
      };

      request.send();
    }

  </script>

  <style>
    .rrplay {
      display: inline-block;
      padding: 10px 20px;
      /*background-color: black;*/
      font-weight: 500;
      color: white;
      font-size: 1.7rem;
      margin-right: 5px;
      letter-spacing: 3px;
      border: 2px solid white;

    }
    .rrdownload {
      display: inline-block;
      padding: 12px;
      font-weight: 500;
      color: white;
      font-size: 1.7rem;
      margin-right: 5px;
      letter-spacing: 3px;
    }
  </style>

</rrbutton>
